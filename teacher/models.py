
from django.db import models


class Teacher(models.Model):
    specialization_choice = (
        ('Mathematics', 'Mathematics'),
        ('English', 'English'),
        ('Science', 'Science'),
        ('Physical Education ', 'Physical Education '),
        ('Art ', 'Art'),
        ('Music ', 'Music'),
        ('Geography', 'Geography'),
        ('History', 'History'),
        ('Computer Science ', 'Computer Science '),
        ('Chemistry', 'Chemistry'),
        ('Biology', 'Biology'),
        ('Biology', 'Biology'),
        ('Nutrition', 'Nutrition'),
        ('Philosophy', 'Philosophy')
    )

    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30)
    specialization = models.CharField(max_length=30, choices=specialization_choice)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
