from django.urls import path
from home import views


urlpatterns = [
    path('page/', views.homepage, name='home'),
    path('all_students/', views.home, name='list of students'),
    path('dream_cars/', views.brands, name='my_cars'),
    path('', views.HomeTemplateView.as_view(), name='homepage')
]
