from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView


def homepage(request):
    return HttpResponse('Hello darkness my old friend!')

@login_required
def home(request):
    context = {
        "all_students": [
            {
                'first_name': 'Claudiu',
                'last_name': 'Nechifor',
                'age': 31
            },
            {
                'first_name': 'Mircea',
                'last_name': 'Popovici',
                'age': 32
            }
        ]
    }
    return render(request, 'home/home.html', context)

def brands(request):
    context = {
        "dream_cars": [
            {
                'Brand': 'Volkswagen Arteon',
                'Production_year': 2017,
                'Engine_capacity': '1,984 cc'
            },
            {
                'Brand': 'Maserati Levante',
                'Production_year': 2016,
                'Engine_capacity': '2,979 cc V6 twin turbo'
            },
            {
                'Brand': 'Shelby Super Snake',
                'Production_year': 2021,
                'Engine_capacity': '2,979 cc'
            }
        ]
    }
    return render(request, 'home/cars.html', context)


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'
